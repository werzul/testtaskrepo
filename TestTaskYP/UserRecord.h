//
//  UserRecord.h
//  TestTaskYP
//
//  Created by werzul on 08/10/14.
//  Copyright (c) 2014 werzul. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Email;

@interface UserRecord : NSManagedObject

@property (nonatomic, retain) NSString * avatarPath;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * personName;
@property (nonatomic, retain) NSNumber * recordID;
@property (nonatomic, retain) NSSet *emails;
@end

@interface UserRecord (CoreDataGeneratedAccessors)

- (void)addEmailsObject:(Email *)value;
- (void)removeEmailsObject:(Email *)value;
- (void)addEmails:(NSSet *)values;
- (void)removeEmails:(NSSet *)values;

@end
