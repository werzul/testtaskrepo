//
//  EmailTableViewCell.m
//  TestTaskYP
//
//  Created by werzul on 08/10/14.
//  Copyright (c) 2014 werzul. All rights reserved.
//

#import "EmailTableViewCell.h"
#import "Email.h"

@interface EmailTableViewCell ()

@property (nonatomic, weak) IBOutlet UILabel *emailLabel;

@end

@implementation EmailTableViewCell

- (void)setEmail:(Email *)email {
    _email = email;
    _emailLabel.text = email.email;
}

@end
