//
//  AppDelegate.h
//  TestTaskYP
//
//  Created by werzul on 07/10/14.
//  Copyright (c) 2014 werzul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
