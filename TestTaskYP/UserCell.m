//
//  UserCell.m
//  TestTaskYP
//
//  Created by werzul on 07/10/14.
//  Copyright (c) 2014 werzul. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "UserCell.h"
#import "UserRecord.h"

@interface UserCell ()

@property (nonatomic, weak) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, weak) IBOutlet UILabel *emailLabel;
@property (nonatomic, weak) IBOutlet UILabel *personNameLabel;

@end

@implementation UserCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.borderWidth = 2.0f;
}

- (void)setUser:(UserRecord *)user {
    _user = user;
    
    _avatarImageView.image = [UIImage imageWithContentsOfFile:user.avatarPath];
    _emailLabel.text = user.email;
    _personNameLabel.text = user.personName;
}

@end
