//
//  UserInfoViewController.h
//  TestTaskYP
//
//  Created by werzul on 08/10/14.
//  Copyright (c) 2014 werzul. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserRecord;

@interface UserInfoViewController : UIViewController

@property (nonatomic, strong) UserRecord *user;

@end
