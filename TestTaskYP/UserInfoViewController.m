//
//  UserInfoViewController.m
//  TestTaskYP
//
//  Created by werzul on 08/10/14.
//  Copyright (c) 2014 werzul. All rights reserved.
//

#import "UserInfoViewController.h"
#import "UserRecord.h"
#import "EmailTableViewCell.h"

@interface UserInfoViewController () <UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *personNameLabel;
@property (nonatomic, weak) NSArray *emails;
@property (nonatomic, weak) UITableView *emailsTableView;

@end

@implementation UserInfoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _avatarImageView.image = [UIImage imageWithContentsOfFile:_user.avatarPath];
    _emails = _user.emails.allObjects;
    _personNameLabel.text = _user.personName;
}



- (BOOL)shouldAutorotate {
    return NO;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _emails.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseEmailIdentifier = @"emailCell";
    EmailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseEmailIdentifier];
    cell.email = _emails[indexPath.row];
    return cell;
}

@end
