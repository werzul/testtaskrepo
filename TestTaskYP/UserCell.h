//
//  UserCell.h
//  TestTaskYP
//
//  Created by werzul on 07/10/14.
//  Copyright (c) 2014 werzul. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserRecord;

@interface UserCell : UICollectionViewCell

@property (nonatomic, strong) UserRecord *user;

@end
