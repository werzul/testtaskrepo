//
//  ABManager.h
//  TestTaskYP
//
//  Created by werzul on 07/10/14.
//  Copyright (c) 2014 werzul. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABManager : NSObject

+ (instancetype)sharedManager;

- (void)updateRecords;

@end
