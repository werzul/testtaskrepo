//
//  ParticipiantsViewController.m
//  TestTaskYP
//
//  Created by werzul on 07/10/14.
//  Copyright (c) 2014 werzul. All rights reserved.
//

#import "ParticipiantsViewController.h"
#import "UserCell.h"
#import "UserRecord.h"
#import "DatabaseManager.h"
#import "ABManager.h"
#import "UserInfoViewController.h"
#import <AddressBook/AddressBook.h>

#define userInfoSegue @"userInfoSegue"

@interface ParticipiantsViewController () <UICollectionViewDataSource, UICollectionViewDelegate, NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *usersCollectionView;
@property (weak, nonatomic) IBOutlet UIView *noAccessView;
@property (nonatomic, strong) NSFetchedResultsController *usersFetchController;

@end

@implementation ParticipiantsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addressBookChangedAccess) name:@"ABStatusGranted" object:nil];
    
    [self setupFetchedController];
    
    [[ABManager sharedManager] updateRecords];
}

- (void)addressBookChangedAccess {
    BOOL denied = ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied;
    _noAccessView.hidden = !denied;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied) {
        _noAccessView.hidden = NO;
    }
}

- (void)setupFetchedController {
    NSManagedObjectContext *context = [[DatabaseManager sharedManager] managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest new];
    request.entity = [NSEntityDescription entityForName:@"UserRecord"
                                     inManagedObjectContext:context];
    request.sortDescriptors = @[];
    request.predicate = [NSPredicate predicateWithFormat:@"emails.@count > 0"];
    NSFetchedResultsController *controller = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                                     managedObjectContext:context
                                                                                       sectionNameKeyPath:nil
                                                                                                cacheName:nil];
    [controller setDelegate:self];
    _usersFetchController = controller;
    NSError *err;
    [controller performFetch:&err];
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _usersFetchController.fetchedObjects.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *userCellIdentifier = @"UserCell";
    UserCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:userCellIdentifier forIndexPath:indexPath];
    
    UserRecord *user = [_usersFetchController objectAtIndexPath:indexPath];
    cell.user = user;
    
    return cell;
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [_usersCollectionView reloadData];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    UserRecord *user = [_usersFetchController objectAtIndexPath:indexPath];
    if (user) { //(user.emails.allObjects.count > 1) has multiple emails
        [self performSegueWithIdentifier:userInfoSegue sender:user];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:userInfoSegue]) {
        UserInfoViewController *vc = segue.destinationViewController;
        vc.user = (UserRecord *)sender;
    }
}

@end
