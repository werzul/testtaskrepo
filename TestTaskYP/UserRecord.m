//
//  UserRecord.m
//  TestTaskYP
//
//  Created by werzul on 08/10/14.
//  Copyright (c) 2014 werzul. All rights reserved.
//

#import "UserRecord.h"
#import "Email.h"


@implementation UserRecord

@dynamic avatarPath;
@dynamic email;
@dynamic personName;
@dynamic recordID;
@dynamic emails;

- (void)setAvatarPath:(NSString *)avatarPath {
    if (![self.avatarPath isEqualToString:avatarPath]) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        BOOL success = [fileManager removeItemAtPath:self.avatarPath error:&error];
        if (success) {
            //whatever
        }
        [self willChangeValueForKey:@"avatarPath"];
        [self setPrimitiveValue:avatarPath forKey:@"avatarPath"];
        [self didChangeValueForKey:@"avatarPath"];
    }
}

- (void)delete:(id)sender {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:self.avatarPath error:&error];
    if (success) {
        //whatever
    }
    [super delete:sender];
}

@end
