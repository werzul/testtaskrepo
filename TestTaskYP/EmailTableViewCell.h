//
//  EmailTableViewCell.h
//  TestTaskYP
//
//  Created by werzul on 08/10/14.
//  Copyright (c) 2014 werzul. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Email;

@interface EmailTableViewCell : UITableViewCell

@property (nonatomic, strong) Email *email;

@end
