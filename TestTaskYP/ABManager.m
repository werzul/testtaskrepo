//
//  ABManager.m
//  TestTaskYP
//
//  Created by werzul on 07/10/14.
//  Copyright (c) 2014 werzul. All rights reserved.
//

#import "ABManager.h"
#import <AddressBook/AddressBook.h>
#import "UserRecord.h"
#import "Email.h"
#import "DatabaseManager.h"

@implementation ABManager

ABAddressBookRef addressBook;

+ (ABManager *)sharedManager {
    static ABManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[ABManager alloc] init];
    });
    return sharedManager;
}

- (id)init {
    self = [super init];
    if (self) {
        addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
        ABAddressBookRegisterExternalChangeCallback(addressBook, addressBookChanged, (__bridge void *)(self));
    }
    return self;
}

- (void)updateRecords {
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined){
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ABStatusGranted" object:nil];
            if (granted){
                [self updateRecords];// go to 2nd round (kABAuthorizationStatusAuthorized)
            }
        });
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        [self importContactsFromAB];
    }
}

- (void)importContactsFromAB {
    NSManagedObjectContext *context = [DatabaseManager sharedManager].managedObjectContext;
    NSFetchRequest *request = [NSFetchRequest new];
    request.entity = [NSEntityDescription entityForName:@"UserRecord"
                                 inManagedObjectContext:context];
    NSError *err;
    NSArray *users = [context executeFetchRequest:request error:&err];
    //get recordIDs for merge
    NSMutableSet *recordIDs = [NSMutableSet set];
    for (UserRecord *user in users) {
        [recordIDs addObject:user.recordID];
    }
    
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBookRef);
    if (allPeople) {
        for (int index = 0; index < CFArrayGetCount(allPeople); index++) {
            ABRecordRef thisPerson = CFArrayGetValueAtIndex(allPeople, index);
            
            NSString *firstName = (__bridge NSString *)ABRecordCopyValue(thisPerson,kABPersonFirstNameProperty);
            if (!firstName.length) {
                firstName = @"";
            }
            NSString *lastName = (__bridge NSString *)ABRecordCopyValue(thisPerson,kABPersonLastNameProperty);
            if (!lastName.length) {
                lastName = @"";
            }
            CFDataRef imageData = ABPersonCopyImageData(thisPerson);
            NSString *imagePath = nil;
            if (imageData) {
                //ABPersonHasImageData
                NSData *data = (NSData *)CFBridgingRelease(imageData);
                NSError *err;
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                NSString * path = [paths  objectAtIndex:0];
                path = [path stringByAppendingPathComponent:[[NSUUID UUID] UUIDString]];
                [data writeToFile:path options:NSDataWritingAtomic error:&err];
                if (!err) {
                    imagePath = path;
                }
            }
            
            ABMultiValueRef emails = ABRecordCopyValue(thisPerson, kABPersonEmailProperty);
            NSMutableSet *emailsSet = [NSMutableSet set];
            for (CFIndex i = 0; i < ABMultiValueGetCount(emails); i++)
            {
                CFStringRef emailRef = ABMultiValueCopyValueAtIndex(emails, i);
                NSString *emailString = (NSString *)CFBridgingRelease(emailRef);
                Email *email = [NSEntityDescription insertNewObjectForEntityForName:@"Email" inManagedObjectContext:context];
                email.email = emailString;
                [emailsSet addObject:email];
            }
            
            int recID = ABRecordGetRecordID(thisPerson);
            
            
            request.predicate = [NSPredicate predicateWithFormat:@"recordID = %@",@(recID)];
            NSError *err;
            NSArray *array = [context executeFetchRequest:request error:&err];
            if (array.count) { //contains
                UserRecord *user = array.firstObject;
                user.avatarPath = imagePath;
                user.personName = [NSString stringWithFormat:@"%@ %@",firstName,lastName];
                [user setEmails:emailsSet];
                [recordIDs removeObject:user.recordID];
            } else {
                UserRecord* user = [NSEntityDescription insertNewObjectForEntityForName:@"UserRecord" inManagedObjectContext:context];
                user.avatarPath = imagePath;
                user.personName = [NSString stringWithFormat:@"%@ %@",firstName,lastName];
                [user setEmails:emailsSet];
                user.recordID = @(recID);
            }
            
            CFRelease(emails);
        }
        CFRelease(allPeople);
    }
    
    request.predicate = [NSPredicate predicateWithFormat:@"recordID in %@",recordIDs];
    NSArray *usersForDelete = [context executeFetchRequest:request error:&err];
    if (usersForDelete.count) { //contains
        for (UserRecord *user in usersForDelete) {
            [context deleteObject:user];
        }
    }
    [[DatabaseManager sharedManager] saveContext];
    if (addressBookRef) {
        CFRelease(addressBookRef);
    }
}

void addressBookChanged(ABAddressBookRef reference,
                         CFDictionaryRef dictionary,
                         void *context) {
    ABManager *manager = (__bridge ABManager *)context;
    [manager updateRecords];
}

- (void)dealloc {
    ABAddressBookUnregisterExternalChangeCallback(addressBook, addressBookChanged, (__bridge void *) self);
    addressBook ? CFRelease(addressBook) : nil;
}

@end
