//
//  Email.h
//  TestTaskYP
//
//  Created by werzul on 08/10/14.
//  Copyright (c) 2014 werzul. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class UserRecord;

@interface Email : NSManagedObject

@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) UserRecord *userRecord;

@end
